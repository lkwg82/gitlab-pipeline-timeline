/** @module GitLab */

/**
 * @typedef  {object} PipelineData
 * @property {string} projectName  The full name of the project (namespace/project).
 * @property {number} pipelineId   The ID of the pipeline.
 */

const msPerSecond = 1000;

/**
 * Returns normalized date, rounded to the nearest second,
 * relative to the start date.
 *
 * @param   {Date} date  The date to be normalized.
 * @param   {Date} start The start date.
 * @returns {Date}       The normalized date.
 * @static
 * @private
 */
const normalizeDate = (date, start) => {
    const normalizedSeconds =
        Math.round((Date.parse(date) - Date.parse(start)) / msPerSecond) *
        msPerSecond;
    return new Date(normalizedSeconds);
};

/**
 * Get Pipeline Url from form.
 *
 * @returns {URL} The URL object.
 * @static
 * @private
 */
const getBasePipelineUrl = () => {
    const pipelineUrlField = document.querySelector('#pipeline-url');
    return new URL(pipelineUrlField.value);
};

/**
 * Get the GitLab API Pipeline URL for the pipeline data.
 *
 * @param   {PipelineData} pipelineData The pipeline data.
 * @returns {string}                    The GitLab API Pipeline URL.
 * @static
 * @private
 */
const getPipelineUrl = ({ projectName, pipelineId }) => {
    const { hostname } = getBasePipelineUrl();
    return `https://${hostname}/api/v4/projects/${encodeURIComponent(
        projectName
    )}/pipelines/${pipelineId}`;
};

const appKey = 'gitlab-pipeline-timeline';
const { localStorage } = window;

if (!localStorage) {
    throw new Error('storage has already been initialized');
}

const fetchWithToken = async (url) => {
    const token = localStorage.getItem(appKey);
    let headers = {};
    if (token) {
        console.log('got token');
        headers = { 'Private-Token': token };
    } else {
        console.log('no token');
    }
    const response = await fetch(url, { headers });
    console.log(response);
    if (response.ok) {
        return response;
    }
    const div = document.querySelector('#hint');
    div.innerHTML = `Error: Could be the wrong url or you need to set a token ( "${appKey}" in local storage)`;
};

/**
 * Get the GitLab API Jobs URL for the pipeline data.
 *
 * @param   {PipelineData} pipelineData The pipeline data.
 * @returns {string}                    The GitLab API Jobs URL.
 * @static
 * @private
 */
const getPipelineJobsUrl = ({ projectName, pipelineId }) =>
    `${getPipelineUrl({
        projectName,
        pipelineId
    })}/jobs?per_page=100`;

/**
 * Get the GitLab API Pipeline Bridges URL for the pipeline data.
 *
 * @param   {PipelineData} pipelineData The pipeline data.
 * @returns {string}                    The GitLab API Pipeline Bridges URL.
 * @static
 * @private
 */
const getPipelineBridgesUrl = ({ projectName, pipelineId }) =>
    `${getPipelineUrl({
        projectName,
        pipelineId
    })}/bridges?per_page=100`;

/**
 * Get the data for all child pipelines for the given project and pipeline.
 *
 * @param   {PipelineData} pipelineData The pipeline data.
 * @returns {object}                    An array of job data.
 * @async
 * @static
 * @private
 */
const getChildPipelineData = async ({ projectName, pipelineId }) => {
    const bridgesUrl = getPipelineBridgesUrl({
        projectName,
        pipelineId
    });
    const bridgesResponse = await fetchWithToken(bridgesUrl);
    const bridgesJson = await bridgesResponse.json();

    if (bridgesJson.length > 0) {
        // This is a simplification to avoid requesting the data
        // from each child pipeline since the times are within one
        // second of the start/end times and they're all rounded.
        return (
            bridgesJson
                // Only return jobs where child pipeline were created
                .filter((pipeline) => pipeline.downstream_pipeline)
                .map((pipeline) => ({
                    name: pipeline.name,
                    // eslint-disable-next-line camelcase -- use API names
                    started_at: pipeline.downstream_pipeline.created_at,
                    // eslint-disable-next-line camelcase -- use API names
                    finished_at: pipeline.downstream_pipeline.updated_at
                }))
        );
    }
    return [];
};

/**
 * Get the data for all jobs and child pipelines for the
 * given project and pipeline.
 *
 * @param   {PipelineData} pipelineData The pipeline data.
 * @returns {object}                    An array of job data.
 * @async
 * @static
 * @public
 */
const getPipelineData = async ({ projectName, pipelineId }) => {
    const jobsUrl = getPipelineJobsUrl({ projectName, pipelineId });

    const mainPipelineResponse = await fetchWithToken(jobsUrl);
    const mainPipelineJson = await mainPipelineResponse.json();

    if (mainPipelineJson.length > 0) {
        const pipelineStartTime = mainPipelineJson[0].pipeline.created_at;
        const childPipelineJobs = await getChildPipelineData({
            projectName,
            pipelineId
        });
        const jobs = [...mainPipelineJson, ...childPipelineJobs];

        return jobs
            .map((job) => [
                job.name,
                normalizeDate(job.started_at, pipelineStartTime),
                normalizeDate(job.finished_at, pipelineStartTime)
            ])
            .sort((a, b) => a[1] - b[1]);
    }
};

/**
 * Validates the project name and pipeline ID.
 *
 * @param  {PipelineData} pipelineData The pipeline data.
 * @throws {TypeError}                 If project name or pipeline ID is invalid.
 * @static
 * @private
 */
const validatePipelineData = ({ projectName, pipelineId }) => {
    if (
        !projectName ||
        !pipelineId ||
        projectName.length === 0 ||
        Number.isInteger(pipelineId)
    ) {
        throw new TypeError('Invalid pipeline URL');
    }
};

/**
 * Get and validate project name and pipeline ID from a pipeline URL.
 *
 * @returns {PipelineData} The pipeline data.
 * @static
 * @public
 */
const getPipelineDataFromUrl = () => {
    const path = getBasePipelineUrl().pathname;
    const pipelineUrlRegex = /\/(?<project>.*)\/-\/pipelines\/(?<pipeline>\d*)/;
    const matches = path.match(pipelineUrlRegex);
    const results = {};
    if (matches) {
        results.projectName = matches.groups.project;
        results.pipelineId = matches.groups.pipeline;
        validatePipelineData(results);
    }
    return results;
};

export { getPipelineData, getPipelineDataFromUrl };
