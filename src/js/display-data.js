/** @module Display-Data */

import { getPipelineData, getPipelineDataFromUrl } from './gitlab.js';

/**
 * @typedef  {object} PipelineData
 * @property {string} projectName  The full name of the project (namespace/project).
 * @property {number} pipelineId   The ID of the pipeline.
 * @private
 */

/**
 * Displays the project name and pipeline ID, with links, on the page.
 *
 * @param {PipelineData} pipelineData The pipeline data.
 * @static
 * @private
 */
const displayPipelineData = ({ projectName, pipelineId }) => {
    const projectNameElement = document.querySelector('#chart-title');
    if (projectNameElement) {
        projectNameElement.innerHTML = `Project: <a href="https://gitlab.com/${projectName}/" target="_blank">${projectName}</a>`;
    }

    const pipelineIdElement = document.querySelector('#pipeline-id');
    if (pipelineIdElement) {
        pipelineIdElement.innerHTML = `Pipeline: <a href="https://gitlab.com/${projectName}/-/pipelines/${pipelineId}" target="_blank">${pipelineId}</a>`;
    }
};

/**
 * Gets the timeline chart data table definition.
 *
 * @param   {object} google Google chart object.
 * @returns {object}        The timeline chart data table.
 * @static
 * @private
 */
const getChartDataTable = (google) => {
    const data = new google.visualization.DataTable();
    data.addColumn({ type: 'string', id: 'Job' });
    data.addColumn({ type: 'date', id: 'Start' });
    data.addColumn({ type: 'date', id: 'End' });
    return data;
};

/**
 * Get the timeline chart options based on number of rows.
 *
 * @param   {number} rowCount The number of rows in teh timeline.
 * @returns {object}          The timeline chart options.
 * @static
 * @private
 */
const getChartOptions = (rowCount) => {
    const trackHeight = 41;
    const chartMarginTracks = 2;
    return {
        height: trackHeight * (rowCount + chartMarginTracks),
        width: '100%',
        timeline: {
            rowLabelStyle: { fontSize: 14 }
        },
        hAxis: { format: 'mm:ss' }
    };
};

/**
 * Retrieves pipeline data and generates chart.
 *
 * @param {object} google Google chart object.
 * @async
 * @static
 * @public
 */
async function displayChart(google) {
    const { projectName, pipelineId } = getPipelineDataFromUrl();

    const data = getChartDataTable(google);
    const pipelineData = await getPipelineData({
        projectName,
        pipelineId
    });
    data.addRows(pipelineData);

    displayPipelineData({ projectName, pipelineId });
    const chartContainer = document.querySelector('#chart-div');
    const chart = new google.visualization.Timeline(chartContainer);
    chart.draw(data, getChartOptions(pipelineData.length));
}

export { displayChart };
