# GitLab Pipeline Timeline

An MVC of a utility to visualize the actual timeline of GitLab CI jobs in a completed pipeline. This can be used to determine unnecessary dependencies, and potential improvements from leveraging the new `medium` and `large` gitlab.com Linux container runners.

**_Note: this utility uses [Google Charts](https://developers.google.com/chart/interactive/docs/gallery/timeline) to generate the timeline and in opening the page your browser will make a request to Google for their script._**

## Usage

Go to the pages site for this project at https://gitlab-ci-utils.gitlab.io/gitlab-pipeline-timeline/, enter the URL for a pipeline, and push the `Get Timeline` button. The namespace/project and pipeline ID will be taken from the URL and the actual timeline of the pipeline will be displayed. The timeline chart includes all jobs, and triggered pipelines (displayed as one job), ordered based on the actual start time, as shown below. All times are rounded to the nearest second.

![timeline screenshot](./assets/timeline-screenshot.png)

Hovering over any job's bar displays a popup with the job's start/end time relative to the start of the pipeline and the duration of the job.

## Limitations

This in initial MCV, and as such has a number of limitations:

- Only timelines for projects on gitlab.com can be viewed (since the API URLs point there).
- Only timelines for publicly visible project pipelines can be viewed (since the page does not authenticate to the GitLab API). This requires:
  - The project visibility must be set to "Public" (in General settings).
  - The project CI/CD permissions must be set to "Everyone With Access" (in General settings).
  - The "Public Pipelines" property must be enabled (in CI/CD settings).
- Only the first 100 jobs and 100 triggered pipelines (i.e. pipeline bridges) are displayed. If the pipeline has more than this, any others will not be displayed, and the pipeline duration only reflects the displayed jobs.
- If a job is retried only the last execution is displayed.
- For short duration jobs where the minute of the start and end times are the same, the popup only shows the seconds (e.g. if the jobs runs from 3:30 - 3:53, the popup will show "30s - 53s" and "Duration: 23 seconds").
- Attempting to display timelines for pipelines that are not complete has shown inconsistent results, and may generate an error and not display the timeline.
- Most errors are written only to the console.
